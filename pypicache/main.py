import argparse
import logging
import json

from pypicache import cache
from pypicache import disk
from pypicache import pypi
from pypicache import server

def main():

    parser = argparse.ArgumentParser(
        description="A PYPI cache",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("config_file", help="Path to the configuration ini file")
    args = parser.parse_args()

    config = json.loads(open(args.config_file,'r').read())
    
    defaults = {
        'address'   : '0.0.0.0',
        'port'      : 8080,
        'debug'     : False,
        'reload'    : False,
        'processes' : 1,
        'prefix'    : "/tmp/pypicache"
    }
    
    for k in set(defaults.keys()) - set(config.keys()):
        config[k] = defaults[k]

    loglevel = logging.DEBUG if config['debug'] else logging.INFO

    logging.basicConfig(
        level=loglevel,
        format="%(asctime)s [%(levelname)s] [%(processName)s-%(threadName)s] [%(name)s] [%(filename)s:%(lineno)d] %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S%z"
    )
    logging.info("Debugging: {!r}".format(config['debug']))
    logging.info("Reloading: {!r}".format(config['reload']))

    pypi_server = pypi.PyPI()
    package_store = disk.DiskPackageStore(config['prefix'])
    package_cache = cache.PackageCache(package_store, pypi_server)
    app = server.configure_app(pypi_server, package_store, package_cache, debug=config['debug'])
    app.run(host=config['address'], port=config['port'], debug=config['debug'], use_reloader=config['reload'], processes=config['processes'])

if __name__ == '__main__':
    main()
